(function ($) {
  function getUserLocation(success) {
  navigator.geolocation.getCurrentPosition(
    function (position) {
      success(position);
    });
}

function geoLocateFailure() {
  console.log("It didn't work");
}

$(function () {
  console.log('Simple Geolocation Ready');
  var locTime = localStorage.getItem('locTime');
  console.log(locTime);
  if ($.isNumeric(locTime) && !(Date.now() - locTime > 3600000)) {

    console.log(!(Date.now() - locTime > 3600000));

  }
  else{
    getUserLocation(geoReverseCode);
  }


});

function locationCheck(addresses) {
// Initialize variables that will hold user context in LocalStorage
  isLA = false;
  isCA = false;
  isUS = false;
  USZIP = false;

  $.each(addresses, function (index, value) {

    console.log(value.address_components[0].short_name);

    switch (value.address_components[0].short_name) {
      case 'Los Angeles':
        isLA = true;
        break;
      case 'CA':
        isCA = true;
        break;
      case 'US':
        isUS = true;
        break;
    }

    if ($.isNumeric(value.address_components[0].short_name) && value.address_components[0].types[0] == 'postal_code') {
      USZIP = value.address_components[0].short_name;
    }

  })
  if (isLA != false) {
    setLocalStore('isLA', isLA);
  }
  if (isCA != false) {
    setLocalStore('isCA', isCA);
  }
  if (isUS != false) {
    setLocalStore('isUS', isUS);
  }
  if (USZIP != false) {
    setLocalStore('USZIP', USZIP);
  }
  setLocalStore('locTime', Date.now());
}

function setLocalStore(key, value) {
  // console.log('LocalStore ' + key + ' ' + value); *debugging
  localStorage.setItem(key, value);
}

function geoReverseCode(position) {
  lat = position.coords.latitude;
  long = position.coords.longitude;
  latlng = {'lat': lat, 'lng': long};
  var geocoder = new google.maps.Geocoder;
  geocoder.geocode({'location': latlng}, function (results) {
    locationCheck(results);
    // console.log(results); *debugging
    // console.log(results[0].formatted_address); *debugging
  });
}

window.initMap = function() {
  // console.log('Google Maps Ready'); *debugging
}

})(jQuery);