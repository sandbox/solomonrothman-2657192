(function ($) {
function getUserLocation(success){
  navigator.geolocation.getCurrentPosition(
    function(position){
      success(position);
    });
}

function geoLocateSuccess(position){
  console.log(position);
  $('#location-holder').html('');
  var coords = position.coords;
  $.each(coords, function(key, value){
    locProp = key+" "+value;
    $('#location-holder').append(locProp+'<br />');

  });
  console.log(coords);
}

function geoLocateFailure(){
  console.log("It didn't work");
}

$(function() {
  $('#geolocate-location').click(function(){
    console.log('geolocate as clicked');
    getUserLocation(geoLocateSuccess);
  });
  $('#reverse-geolocate-location').click(function(){
    console.log('geolocate as clicked');
    getUserLocation(geoReverseCode);
  });

  var locTime =  localStorage.getItem('locTime');
  if ($.isNumeric(locTime) && (Date.now() - locTime < 3600000)) {
    console.log('check passed');
    $('#isLA-value').html('In Los Angeles: ' + localStorage.getItem('isLA'));
    $('#isCA-value').html('In California: ' + localStorage.getItem('isCA'));
    $('#isUS-value').html('In US: ' + localStorage.getItem('isUS'));
    $('#isZip-value').html('ZIP: ' + localStorage.getItem('USZIP'));
  }
  else{
    getUserLocation(geoReverseCode);
  }
});

function locationCheck(addresses){

  console.log('about to check locations');
  console.log(addresses);
  isLA = false;
  isCA = false;
  isUS = false;
  USZIP = false;

  $.each(addresses, function (index, value){

    console.log(value.address_components[0].short_name);

    switch(value.address_components[0].short_name){
      case 'Los Angeles':
        isLA = true;
        break;
      case 'CA':
        isCA = true;
        break;
      case 'US':
        isUS = true;
        break;
    }

    if ($.isNumeric(value.address_components[0].short_name) && value.address_components[0].types[0] == 'postal_code'){
      USZIP = value.address_components[0].short_name;
    }

  })
  if(isLA != false){
    setLocalStore('isLA', isLA);
  }
  if(isCA != false){
    setLocalStore('isCA', isCA);
  }
  if(isUS != false){
    setLocalStore('isUS', isUS);
  }
  if(USZIP != false){
    setLocalStore('USZIP', USZIP);
  }
  setLocalStore('locTime', Date.now());
}

function setLocalStore(key, value){
  console.log('LocalStore ' + key + ' ' + value);
  localStorage.setItem(key, value);
}

function geoReverseCode(position){
  $('#reverse-location-holder').html('');
  lat = position.coords.latitude;
  long = position.coords.longitude;
  latlng = {'lat': lat, 'lng': long};
  var geocoder = new google.maps.Geocoder;
  geocoder.geocode({'location': latlng},function(results){
    locationCheck(results);
    console.log(results);
    console.log(results[0].formatted_address);
    $('#reverse-location-holder').append(results[0].formatted_address);
  });
}

window.initMap = function(){
  console.log('Google Maps Ready');
}
})(jQuery);