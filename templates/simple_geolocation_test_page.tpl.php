<h1>Geolocation data here</h1>

 <div>
     <ul>
         <li id="isUS-value">In Los Angeles: </li>
         <li id="isCA-value">In California: </li>
         <li id="isLA-value">In US: </li>
         <li id="isZip-value">Is Zip: </li>
     </ul>
 </div>

 <button id="geolocate-location" name="geolocate-location">From Device</button>
  <div id="location-holder">
      Location is loading
  </div>
  <h1>Reverse Geolocation</h1>
 <button id="reverse-geolocate-location" name="reverse-geolocate-location">Reverse Geolocation</button>
  <div id="reverse-location-holder">
      Location is loading
  </div>
 <h1>Remember Geolocation from Local Storage for Some Browsers</h1>
 <div id="location-holder-remnobrow">
     Location is loading
 </div>

 <h1>Remember Geolocation from Local Storage for All Browsers</h1>
 <div id="location-holder-rem">
     Location is loading
 </div>